<?php
// Set the namespaces so we don't worry about any class name conflicts with other stuff
namespace Lever;

// We also use Guzzle to simplify our remote requests to the Lever API
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class Lever {

    // Class private vars and constants
    private $site = 'leverdemo';
    private $ttl = 3600;
    private $cacheAvailable = false;

    const BASEURI = 'https://api.lever.co/v0/postings';
    const KEY = 'lever_postings';

    // Called when creating an instance, allows us to override the default values
    public function __construct($site = 'leverdemo', $ttl = 3600) {
        $this->site = $site;
        $this->ttl = $ttl;
        $this->cacheAvailable = function_exists('apcu_add');
    }

    /* The public API of our class */

    // Get all the postings (doesn't support paging)
    public function getPostings($skipCache = false) {

        // First check the cache
        $postings = (($this->cacheAvailable && !$skipCache) ? apcu_fetch(self::KEY) : false);

        if ($postings === false) {
            // Was not in cache or cache not available

            // I guess we have to get it from the remote now, so slow
            $response = $this->getFromRemote();

            if ($this->cacheAvailable && $response['success']) {
                // We got the data, so store the postings in cache for next time, so fast, if available
                apcu_add(self::KEY, $response['data'], $ttl);
            }

            // Return either the successful pull or the error
            return $response;
        } else {
            // Was in cache, so return the data
            return [
                'success' => true,
                'data' => $postings
            ];
        }
    }

    // Get one posting by ID value
    public function getPosting($id, $skipCache = false) {

        // Since the all postings request downloads everything already, let's get what we have
        $postings = $this->getPostings($skipCache);

        // Did we get them?
        if ($postings['success']) {

            // Got postings, is this ID in it?
            if (array_key_exists($id, $postings['data']['postings'])) {
                // Found it!
                return [
                    'success' => true,
                    'data' => $postings['data']['postings'][$id]
                ];

            } else {
                // Didn't find, so sad
                $error = "Couldn't find that ID in the postings array";
            }
        } else {
            // Since the get all postings failed, just bubble that error up here, too
            $error = $postings['error'];
        }

        // Hang head in shame, failed, #sad
        return [
            'success' => false,
            'error' => $error
        ];
    }

    public function clearCache() {
        if ($cacheAvailable) {
            apcu_delete(self::KEY);
        }
    }

    /* Our internal functions */

    // Retrieve data from Lever's API
    private function getFromRemote() {

        // We are using this API:
        // https://github.com/lever/postings-api

        // Need a Guzzle client for keeping it simpler
        $client = new Client([
            'base_uri' => self::BASEURI.'/'.$this->site.'/'
        ]);

        // Use Guzzle to get a response, just getting all the postings in one go
        $response = $client->get(
            '', [
                'query' => [
                    'mode' => 'json',
                    'group' => 'location'
                ],
            ]
        );

        // Success is a 200 OK status code
        $statusCode = $response->getStatusCode();
        if ($statusCode = 200) {

            // Extract the postings data
            $body = $response->getBody();
            $json = $body->getContents();

            // Convert the postings data from JSON to an array of associative arrays (each is a posting)
            $data = json_decode($json, true);

            // Reorganize our data so that the keys are posting IDs instead of zero-based index
            // We do this so that our search for a specific posting is super-fast
            $postings = [];
            foreach($data as &$location) {
                foreach ($location['postings'] as &$posting) {
                    $postings[$posting['id']] = $posting;
                    $posting = $posting['id'];
                }
            }
            $output['postings'] = $postings;
            $output['locations'] = $data;

            // Ship it!
            return [
                'success' => true,
                'data' => $output
            ];

        } else {

            // We failed to retrieve from remote, report that
            return  [
                'success' => false,
                'error' => "We weren't able to get the data"
            ];
        }
    }
}
